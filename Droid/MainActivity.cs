﻿using System;

using Xamarin.Forms;
using SQLite.Net;
using Android.Graphics;
using Android.Content;
using Android.Content.PM;
using Android.OS;
using Android.Provider;
using Android.App;

using Uri = Android.Net.Uri;
using Environment = Android.OS.Environment;
using System.Collections.Generic;

[assembly: Xamarin.Forms.Dependency (typeof (JeNaiJamais.Droid.SQLite_Android))]
[assembly: Xamarin.Forms.Dependency (typeof (JeNaiJamais.Droid.MainActivity))]
namespace JeNaiJamais.Droid
{
	[Activity(Label = "JeNaiJamais", Icon = "@drawable/icon", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
	public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity, ICamera
	{

		protected override void OnCreate(Bundle bundle)
		{
			base.OnCreate(bundle);

			Forms.Init(this, bundle);

			LoadApplication(new App());
		}

		protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
		{
			base.OnActivityResult(requestCode, resultCode, data);

			// make it available in the gallery
			Intent mediaScanIntent = new Intent(Intent.ActionMediaScannerScanFile);
			Uri contentUri = Uri.FromFile(PhotoFile._file);
			mediaScanIntent.SetData(contentUri);
			SendBroadcast(mediaScanIntent);

			// display in ImageView. We will resize the bitmap to fit the display
			// Loading the full sized image will consume to much memory 
			// and cause the application to crash.
			int height = Resources.DisplayMetrics.HeightPixels;
			int width = Resources.DisplayMetrics.WidthPixels;
			PhotoFile.bitmap = PhotoFile._file.Path.LoadAndResizeBitmap (width, height);
		}

		#region ICamera implementation

		public void TakePhoto()
		{
			if (IsThereAnAppToTakePictures())
			{
				CreateDirectoryForPictures();
				TakeAPicture();
			}
		}

		#endregion

		private void TakeAPicture()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);

			PhotoFile._file = new Java.IO.File(PhotoFile._dir, String.Format("myPhoto_{0}.jpg", Guid.NewGuid()));

			intent.PutExtra(MediaStore.ExtraOutput, Uri.FromFile(PhotoFile._file));

			((Activity)Forms.Context).StartActivityForResult(intent, 0);
		}

		private void CreateDirectoryForPictures()
		{
			PhotoFile._dir = new Java.IO.File(Environment.GetExternalStoragePublicDirectory(Environment.DirectoryPictures), "JeNaiJamais");
			if (!PhotoFile._dir.Exists())
			{
				PhotoFile._dir.Mkdirs();
			}
		}

		private bool IsThereAnAppToTakePictures()
		{
			Intent intent = new Intent(MediaStore.ActionImageCapture);
			IList<ResolveInfo> availableActivities = Forms.Context.PackageManager.QueryIntentActivities(intent, PackageInfoFlags.MatchDefaultOnly);
			return availableActivities != null && availableActivities.Count > 0;
		}

	}
		
	public class SQLite_Android : ISQLite {
		public SQLiteConnection GetConnection () {
			var sqliteFilename = "JeNaiJamaisSQLite.db3";
			string documentsPath = System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal); // Documents folder
			var path = System.IO.Path.Combine(documentsPath, sqliteFilename);
			// Create the connection
			var plat = new SQLite.Net.Platform.XamarinAndroid.SQLitePlatformAndroid();
			var conn = new SQLite.Net.SQLiteConnection(plat, path);
			// Return the database connection 
			return conn;
		}
	}

	public static class PhotoFile {
		public static Java.IO.File _file;
		public static Java.IO.File _dir;     
		public static Bitmap bitmap;
	}
}

