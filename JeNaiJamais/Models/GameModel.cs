﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace JeNaiJamais
{
	[Table("Game")]
	public class GameModel
	{
		public GameModel() {
		}

		public GameModel(DateTime time) {
			Date = time;
		}

		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		public DateTime Date { get; set; }
	}
}

