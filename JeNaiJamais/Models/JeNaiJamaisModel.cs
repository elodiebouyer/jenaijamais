﻿using SQLite.Net.Attributes;

namespace JeNaiJamais
{
	[Table("JNJ")]
	public class JeNaiJamaisModel 
	{

		public JeNaiJamaisModel() {}

		public JeNaiJamaisModel(string sentence, int yes, int no) 
		{
			Sentence = sentence;
			NumberNo = no;
			NumberYes = yes;
		}

		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		[Unique]
		public string Sentence{ get; set;}

		public int NumberYes{ get; set; }

		public int NumberNo{ get; set; }

		public int Categorie{ get; set; }

		[Ignore]
		public string Yes 
		{ 
			get 
			{ 
				return NumberYes.ToString(); 
			} 
		}
		[Ignore]
		public string No 
		{ 
			get 
			{ 
				return NumberNo.ToString(); 
			}
		}

		[Ignore]
		public string Total 
		{ 
			get 
			{ 
				return (NumberNo + NumberYes).ToString(); 
			}
		}
	}
}

