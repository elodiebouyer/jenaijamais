﻿using System;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace JeNaiJamais
{
	[Table("JNJGame")]
	public class JnpGameModel
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		[ForeignKey(typeof(GameModel))]
		public int GameId { get; set; }

		[ForeignKey(typeof(JeNaiJamaisModel))]
		public int JnpId{ get; set; }
	}
}

