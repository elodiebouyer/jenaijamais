﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace JeNaiJamais
{
	[Table("PlayerGame")]
	public class PlayerGameModel
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		[ForeignKey(typeof(GameModel))]
		public int GameId { get; set; }

		[ForeignKey(typeof(PlayerModel))]
		public int PlayerId{ get; set; }
	}
}

