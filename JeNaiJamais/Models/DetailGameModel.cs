﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

namespace JeNaiJamais
{
	[Table("DetailGame")]
	public class DetailGameModel
	{
		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		[ForeignKey(typeof(GameModel))]
		public int GameId { get; set; }

		[ForeignKey(typeof(PlayerModel))]
		public int PlayerId{ get; set; }

		[ForeignKey(typeof(JeNaiJamaisModel))]
		public int JnjId{ get; set; }

		public bool answer { get; set; }
	}

	public class Detail
	{
		public string Name { get; set; }
		public string Answer { get; set; } 
	}
}

