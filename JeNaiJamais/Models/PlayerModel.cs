﻿using SQLite.Net.Attributes;

namespace JeNaiJamais
{
	[Table("Player")]
	public class PlayerModel
	{

		public PlayerModel() { }

		public PlayerModel(string name)
		{
			Name = name;
		}

		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		[Unique]
		public string Name{ get; set;}
	}
}

