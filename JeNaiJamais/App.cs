﻿using Storm.Mvvm;
using Xamarin.Forms;

namespace JeNaiJamais
{
	public class App : MvvmApplication<HomePage>
	{
		public App()
			: base(() =>
				{
					DependencyService.Register<IGameService, GameService>();
					DependencyService.Register<IDatabaseService, DatabaseService>();
				})
		{

		}
	}
}

