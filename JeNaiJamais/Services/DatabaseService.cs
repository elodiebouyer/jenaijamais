﻿using SQLite.Net;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Collections.Generic;
using System;

namespace JeNaiJamais
{
	public class DatabaseService : IDatabaseService
	{
		private SQLiteConnection database;

		public DatabaseService()
		{
			database = DependencyService.Get<ISQLite>().GetConnection();
			database.CreateTable<PlayerModel>();
			database.CreateTable<JeNaiJamaisModel>();
			database.CreateTable<GameModel>();
			database.CreateTable<PlayerGameModel>();
			database.CreateTable<JnpGameModel>();	
			database.CreateTable<DetailGameModel>();

			List<JeNaiJamaisModel> l = new List<JeNaiJamaisModel>(database.Table<JeNaiJamaisModel>());
			if (l.Count == 0)
			{
				createJnJ();
				createJnJ2();
			}
		}
			
		private void createJnJ()
		{
			JeNaiJamaisModel j1 = new JeNaiJamaisModel();
			j1.Sentence = "Je n'ai jamais joué au jeu \"Je n'ai jamais\".";
			j1.Categorie = 1;
			database.Insert(j1);

			JeNaiJamaisModel j2 = new JeNaiJamaisModel();
			j2.Sentence = "Je n'ai jamais dormi dans une voiture.";
			j2.Categorie = 1;
			database.Insert(j2);

			JeNaiJamaisModel j3 = new JeNaiJamaisModel();
			j3.Sentence = "Je n'ai jamais oublié les cles de chez moi.";
			j3.Categorie = 1;
			database.Insert(j3);

			JeNaiJamaisModel j4 = new JeNaiJamaisModel();
			j4.Sentence = "Je n'ai jamais parlé durant mon someil.";
			j4.Categorie = 1;
			database.Insert(j4);

			JeNaiJamaisModel j5 = new JeNaiJamaisModel();
			j5.Sentence = "Je n'ai jamais sonné à une porte en me marrant.";
			j5.Categorie = 1;
			database.Insert(j5);

			JeNaiJamaisModel j6 = new JeNaiJamaisModel();
			j6.Sentence = "Je n'ai jamais regardé friends.";
			j6.Categorie = 1;
			database.Insert(j6);

			JeNaiJamaisModel j7 = new JeNaiJamaisModel();
			j7.Sentence = "Je n'ai jamais fait la vaiselle en maillot de bain.";
			j7.Categorie = 1;
			database.Insert(j7);

			JeNaiJamaisModel j8 = new JeNaiJamaisModel();
			j8.Sentence = "Je n'ai jamais gagné aux jeux de grattage.";
			j8.Categorie = 1;
			database.Insert(j8);

			JeNaiJamaisModel j9 = new JeNaiJamaisModel();
			j9.Sentence = "Je n'ai jamais mangé d'huitres.";
			j9.Categorie = 1;
			database.Insert(j9);

			JeNaiJamaisModel j10 = new JeNaiJamaisModel();
			j10.Sentence = "Je n'ai jamais fait plus de 5 magasins en une journée.";
			j10.Categorie = 1;
			database.Insert(j10);
		}

		private void createJnJ2()
		{
			JeNaiJamaisModel j1 = new JeNaiJamaisModel();
			j1.Sentence = "Je n'ai jamais raconté mes exploits sexuels.";
			j1.Categorie = 2;
			database.Insert(j1);

			JeNaiJamaisModel j2 = new JeNaiJamaisModel();
			j2.Sentence = "Je n'ai jamais regretté des actes sexuels";
			j2.Categorie = 2;
			database.Insert(j2);

			JeNaiJamaisModel j3 = new JeNaiJamaisModel();
			j3.Sentence = "Je n'ai jamais embrassé un miroir.";
			j3.Categorie = 2;
			database.Insert(j3);

			JeNaiJamaisModel j4 = new JeNaiJamaisModel();
			j4.Sentence = "Je n'ai jamais été menottée.";
			j4.Categorie = 2;
			database.Insert(j4);

			JeNaiJamaisModel j5 = new JeNaiJamaisModel();
			j5.Sentence = "Je n'ai jamais suivi quelqu'un.";
			j5.Categorie = 2;
			database.Insert(j5);

			JeNaiJamaisModel j6 = new JeNaiJamaisModel();
			j6.Sentence = "Je n'ai jamais été choqué par les expériences sexuelles de mes amis.";
			j6.Categorie = 2;
			database.Insert(j6);

			JeNaiJamaisModel j7 = new JeNaiJamaisModel();
			j7.Sentence = "Je n'ai jamais menti sur ma personnalité.";
			j7.Categorie = 2;
			database.Insert(j7);

			JeNaiJamaisModel j8 = new JeNaiJamaisModel();
			j8.Sentence = "Je n'ai jamais été recalée en boîte de nuit.";
			j8.Categorie = 2;
			database.Insert(j8);

			JeNaiJamaisModel j9 = new JeNaiJamaisModel();
			j9.Sentence = "Je n'ai jamais fait de test de grossesse.";
			j9.Categorie = 1;
			database.Insert(j9);

			JeNaiJamaisModel j10 = new JeNaiJamaisModel();
			j10.Sentence = "Je n'ai jamais voulu sortir avec quelqu'un par intérêt.";
			j10.Categorie = 2;
			database.Insert(j10);
		}

		/**
		 * Add or return if exist player to the database.
		 */
		public PlayerModel insertPlayer(string name)
		{
			List<PlayerModel> playerLst	= database.Query<PlayerModel>("SELECT * FROM Player WHERE Name = ?", name);

			if (playerLst != null)
			{
				foreach(PlayerModel p in playerLst)
				{
					return p;
				}
			}
			PlayerModel player = new PlayerModel(name);
			database.Insert(player);
			return player;
		}

		public int countPlayer()
		{
			return database.Table<PlayerModel>().Count();
		}

		/**
		 * Create a new game.
		 */ 
		public int newGame(ObservableCollection<PlayerModel> players)
		{
			GameModel game = new GameModel(DateTime.Now);
			database.Insert(game);

			foreach(PlayerModel player in players)
			{
				PlayerGameModel pgm = new PlayerGameModel();
				pgm.GameId = game.Id;
				pgm.PlayerId = player.Id;
			}
			return game.Id;
		}

		public bool newJnp(string sentence)
		{
			bool ok = false;
			try
			{
				database.Insert(new JeNaiJamaisModel(sentence, 0, 0));
				ok = true;
			}
			catch(SQLiteException)
			{
				ok = false;
			}
			return ok;
		}

		public ObservableCollection<JeNaiJamaisModel> getAllJeNaiJamais(string mode)
		{
			if (mode == null)
			{
				return new ObservableCollection<JeNaiJamaisModel>(database.Table<JeNaiJamaisModel>());
			}
			else if (mode.Equals("Soft"))
			{
				return new ObservableCollection<JeNaiJamaisModel>(database.Query<JeNaiJamaisModel>("SELECT * FROM JNJ WHERE Categorie = ?", 1));
			}

			return new ObservableCollection<JeNaiJamaisModel>(database.Query<JeNaiJamaisModel>("SELECT * FROM JNJ WHERE Categorie = ?", 2));
		}
			
		public ObservableCollection<GameModel> getAllGame()
		{
			return new ObservableCollection<GameModel>(database.Table<GameModel>());
		}

		public void addJnpToGame(JeNaiJamaisModel jnp, int idGame)
		{
			JnpGameModel jnpGm = new JnpGameModel();
			jnpGm.GameId = idGame;
			jnpGm.JnpId = jnp.Id;
			database.Insert(jnpGm);
		}

		public void addAnswer(PlayerModel player, int gameid, string jnj, bool ans)
		{
			List<JeNaiJamaisModel> jnjList = database.Query<JeNaiJamaisModel>("SELECT * FROM JNJ WHERE Sentence = ?", jnj);
			if (jnjList != null && jnjList.Count > 0) 
			{
				JeNaiJamaisModel jnjModel = jnjList[0];
				if (ans)
					jnjModel.NumberYes++;
				else
					jnjModel.NumberNo++;
				database.Update(jnjModel);

				DetailGameModel detail = new DetailGameModel();
				detail.answer = ans;
				detail.GameId = gameid;
				detail.JnjId = jnjModel.Id;
				detail.PlayerId = player.Id;
				database.Insert(detail);
			}
		}

		/**
		 * Delete all table PlayerGame, JnpGame, DetailGame and Game table.
		 */ 
		public void resetAllHisto()
		{
			database.DeleteAll<PlayerGameModel>();
			database.DeleteAll<JnpGameModel>();
			database.DeleteAll<DetailGameModel>();
			database.DeleteAll<GameModel>();
		}

		public void resetAllJnjStat()
		{
			List<JeNaiJamaisModel> jnjList = new List<JeNaiJamaisModel>(database.Table<JeNaiJamaisModel>());
			foreach (JeNaiJamaisModel jnj in jnjList)
			{
				jnj.NumberNo = 0;
				jnj.NumberYes = 0;
				database.Update(jnj);
			}
		}


		public ObservableCollection<Detail> getDetailOneJnjGame(int idGame, int idJnj) {
			ObservableCollection<DetailGameModel> details = new ObservableCollection<DetailGameModel>( database.Query<DetailGameModel>("SELECT * FROM DetailGame WHERE GameId = ? AND JnjId = ?", idGame, idJnj));
			ObservableCollection<Detail> detailOneJnjGame = new ObservableCollection<Detail>();

			foreach (DetailGameModel dg in details)
			{
				Detail d = new Detail();
				if (dg.answer)
					d.Answer = "Oui";
				else
					d.Answer = "Non";
				d.Name = database.Get<PlayerModel>(dg.PlayerId).Name;
				detailOneJnjGame.Add(d);
			}
			return detailOneJnjGame;
		}

		public ObservableCollection<JeNaiJamaisModel> getListjnjGame(int idGame)
		{
			ObservableCollection<DetailGameModel> details = new ObservableCollection<DetailGameModel>( database.Query<DetailGameModel>("SELECT * FROM DetailGame WHERE GameId = ?", idGame));
			ObservableCollection<JeNaiJamaisModel> detailLst = new ObservableCollection<JeNaiJamaisModel>();
			foreach (DetailGameModel dg in details)
			{
				JeNaiJamaisModel jnj = database.Get<JeNaiJamaisModel>(dg.JnjId);
				if( !containJnp(detailLst, jnj.Id) )
					detailLst.Add(jnj);
			}
			return detailLst;
		}

		private bool containJnp(ObservableCollection<JeNaiJamaisModel> lst, int id)
		{
			foreach (JeNaiJamaisModel j in lst)
			{	
				if (j.Id == id)
					return true;
			}
			return false;
		}
	}
}

