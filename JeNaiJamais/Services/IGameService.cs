﻿using System.Collections.ObjectModel;

namespace JeNaiJamais
{
	public interface IGameService
	{
		int getIdGame();

		void addPlayer(string playerName);

		int countPlayer();

		ObservableCollection<PlayerModel> getList();
		ObservableCollection<GameModel> getListGame();

		ObservableCollection<JeNaiJamaisModel> getListJnjGame(int idGame);

		string nextPlayer();
		string nextJNP();

		void startGame(string mode);

		void addAnswer(PlayerModel player, string jnj, bool ans);

		void resetAllHisto();
		void resetAllJnjStat();
		ObservableCollection<Detail> getDetailOneJnjGame(int idGame,int idJnj);
	}
}

