﻿using System;
using System.Collections.ObjectModel;

namespace JeNaiJamais
{
	public interface IDatabaseService
	{
		PlayerModel insertPlayer(string name);
		int countPlayer();
		int newGame(ObservableCollection<PlayerModel> players);
		bool newJnp(string sentence);
		ObservableCollection<JeNaiJamaisModel> getAllJeNaiJamais(string mode);
		ObservableCollection<GameModel> getAllGame();
		ObservableCollection<JeNaiJamaisModel> getListjnjGame(int idGame);
		void addJnpToGame(JeNaiJamaisModel jnp, int idGame);
		void addAnswer(PlayerModel player, int gameid, string jnj, bool ans);
		void resetAllHisto();
		void resetAllJnjStat();
		ObservableCollection<Detail> getDetailOneJnjGame(int idGame, int idJnj);
	}
}

