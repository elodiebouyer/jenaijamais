﻿using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace JeNaiJamais
{
	public class GameService : IGameService
	{
		private ObservableCollection<PlayerModel> mPlayers;
		private ObservableCollection<JeNaiJamaisModel> mJNJ;

		public int GameId { get; set; }

		public GameService()
		{
			mPlayers = new ObservableCollection<PlayerModel>();
		}

		public int getIdGame()
		{
			return GameId;
		}

		public void startGame(string mode)
		{
			mJNJ = DependencyService.Get<IDatabaseService>().getAllJeNaiJamais(mode);
			GameId = DependencyService.Get<IDatabaseService>().newGame(mPlayers);
		}

		public void addPlayer(string playerName)
		{
			PlayerModel player = DependencyService.Get<IDatabaseService>().insertPlayer(playerName);
			mPlayers.Add(player);
		}

		public int countPlayer()
		{
			return mPlayers.Count;
		}

		public ObservableCollection<PlayerModel> getList()
		{
			return mPlayers;
		}

		public ObservableCollection<GameModel> getListGame()
		{
			return DependencyService.Get<IDatabaseService>().getAllGame();
		}

		public string nextPlayer()
		{
			Random rd = new Random();
			return mPlayers[rd.Next(0, mPlayers.Count)].Name;
		}

		public string nextJNP()
		{
			Random rd = new Random();
			JeNaiJamaisModel jnp = mJNJ[rd.Next(0, mJNJ.Count)];
			DependencyService.Get<IDatabaseService>().addJnpToGame(jnp, GameId);
			return jnp.Sentence;
		}

		public void addAnswer(PlayerModel player, string jnj, bool ans)
		{
			DependencyService.Get<IDatabaseService>().addAnswer(player, GameId, jnj, ans);
		}

		public void resetAllHisto()
		{
			DependencyService.Get<IDatabaseService>().resetAllHisto();
		}

		public void resetAllJnjStat()
		{
			DependencyService.Get<IDatabaseService>().resetAllJnjStat();
		}
			
		public ObservableCollection<JeNaiJamaisModel> getListJnjGame(int idGame)
		{
			return DependencyService.Get<IDatabaseService>().getListjnjGame(idGame);
		}

		public ObservableCollection<Detail> getDetailOneJnjGame(int idGame, int idJnj)
		{
			return DependencyService.Get<IDatabaseService>().getDetailOneJnjGame(idGame, idJnj);
		}

	}
}

