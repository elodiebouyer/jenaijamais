﻿using System;
using Xamarin.Forms;
using Storm.Mvvm;
using System.Windows.Input;

namespace JeNaiJamais
{
	public class AddJnpViewModel : ViewModelBase
	{
		private string mJnp;
		private static string START_JNP = "Je n'ai jamais";

		public AddJnpViewModel()
		{
			Jnp = AddJnpViewModel.START_JNP;
		}

		public string Jnp
		{ 
			get { return mJnp; }
			set { SetProperty<string>(ref mJnp, value); }
		}

		public ICommand AddJnpCommand 
		{ 
			get { return new Command(AddAction); } 
		}

		public void AddAction()
		{
			if (Jnp.StartsWith(START_JNP) && Jnp.Length > START_JNP.Length)
			{
				if (DependencyService.Get<IDatabaseService>().newJnp(Jnp))
				{
					NavigationService.PopAsync();
				}
				else
				{
					// La JNP existe déjà, afficher un message d'erreur !
				}
			}
			else
			{
				// Afficher message erreur : Une jnp doit commencer par "je n'ai jamais" et doit contenir une suite.
			}
		}
	}
}


