﻿using System;
using Storm.Mvvm;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace JeNaiJamais
{
	public class HistoViewModel : ViewModelBase
	{
		private ObservableCollection<GameModel> mListgame;
		private GameModel mSelectedGame;

		public HistoViewModel()
		{
			ListGame = DependencyService.Get<IGameService>().getListGame();
		}

		public ObservableCollection<GameModel> ListGame
		{
			get { return mListgame; }
			set { SetProperty<ObservableCollection<GameModel>>(ref mListgame, value); }
		}

		public ICommand ResetCommand 
		{ 
			get { return new Command(resetAction); } 
		}

		private void resetAction()
		{
			DependencyService.Get<IGameService>().resetAllHisto();
			ListGame = DependencyService.Get<IGameService>().getListGame();
		}

		public GameModel SelectedGame
		{
			get { return mSelectedGame; }
			set
			{
				if (SetProperty<GameModel>(ref mSelectedGame, value))
				{
					if (value != null)
					{
						OnItemSelected(value);

					}
				}
			}
		}

		private void OnItemSelected(GameModel game)
		{
			ObservableCollection<JeNaiJamaisModel> listDetail = new ObservableCollection<JeNaiJamaisModel>();
			listDetail = DependencyService.Get<IGameService>().getListJnjGame(game.Id);

			NavigationService.PushAsync<DetailHistoPage>(new Dictionary<string, object>() { {"ListDetail", listDetail}, {"GameId", game.Id} });				
		}
	}
}

