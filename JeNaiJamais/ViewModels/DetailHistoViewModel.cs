﻿using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace JeNaiJamais
{
	public class DetailHistoViewModel : ViewModelBase
	{
		private ObservableCollection<JeNaiJamaisModel> mListDetail;
		private ObservableCollection<Detail> mlistInfo;
		private JeNaiJamaisModel mJnjSelected;
		private int mGameId;

		[NavigationParameter]
		public ObservableCollection<JeNaiJamaisModel> ListDetail
		{
			get { return mListDetail; }
			set	{ SetProperty<ObservableCollection<JeNaiJamaisModel>>(ref mListDetail, value);}
		}

		[NavigationParameter]
		public int GameId
		{
			get { return mGameId; }
			set	{ SetProperty<int>(ref mGameId, value);}
		}

		public ObservableCollection<Detail> Detail
		{
			get { return mlistInfo; }
			set	{ SetProperty<ObservableCollection<Detail>>(ref mlistInfo, value);}
		}

		public JeNaiJamaisModel SelectedJnj
		{
			get { return mJnjSelected; }
			set
			{
				if (SetProperty<JeNaiJamaisModel>(ref mJnjSelected, value))
				{
					if (value != null)
					{
						Detail = DependencyService.Get<IGameService>().getDetailOneJnjGame(GameId, value.Id);
					}
				}
			}
		}
	}
}

