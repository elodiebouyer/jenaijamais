﻿using Storm.Mvvm;
using Xamarin.Forms;
using System.Windows.Input;
using System.Collections.ObjectModel;

namespace JeNaiJamais
{
	public class InitPlayViewModel : ViewModelBase
	{
		private string mCurrentName;
		private int mNbPlayer;
		private string mMode;

		public InitPlayViewModel()
		{
			NbPlayer = DependencyService.Get<IGameService>().countPlayer();
			Mode = "Soft";
		}

		/**
		 * When a user edit a new player name.
		 */
		public string PlayerName
		{
			get { return mCurrentName; }
			set { SetProperty<string>(ref mCurrentName, value);}
		}

		public int NbPlayer
		{
			get { return mNbPlayer; }
			set { SetProperty<int>(ref mNbPlayer, value);}
		}

		public string Mode
		{
			get { return mMode; }
			set { SetProperty<string>(ref mMode, value);}
		}

		public ObservableCollection<PlayerModel> ListName
		{
			get { return DependencyService.Get<IGameService>().getList(); }
		}

		/**
		 * When a user click in add button.
		 */ 
		public ICommand AddPlayerCommand 
		{ 
			get { return new Command(AddPlayerAction); } 
		}

		/**
		 * When a user click in start button.
		 */ 
		public ICommand StartCommand 
		{ 
			get { return new Command(StartAction); } 
		}

		public ICommand ChangeModeCommand 
		{ 
			get { return new Command(ModeAction); } 
		}

		/**
		 * Add a new player in the players list.
		 */
		private void AddPlayerAction()
		{
			if (mCurrentName.Equals(""))
				return;

			DependencyService.Get<IGameService>().addPlayer(mCurrentName);NbPlayer = DependencyService.Get<IGameService>().countPlayer();
			PlayerName = "";
		}
			
		private void ModeAction()
		{
			if (Mode.Equals("Soft"))
			{
				Mode = "Sexy";
			}
			else if (Mode.Equals("Sexy"))
			{
				Mode = "Soft";
			}
		}


		/**
		 * Start game.
		 */
		private void StartAction()
		{
			if (NbPlayer < 1)
			{
				//DisplaAlert("Erreur", "Vous devez ajouter plus de joueur pour commencer une partie.", "OK");
				return;
			}
			DependencyService.Get<IGameService>().startGame(Mode);
			NavigationService.PushAsync<PlayPage>();
		}
			
	}
}

