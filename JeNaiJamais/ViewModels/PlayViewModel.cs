﻿using Storm.Mvvm;

using Xamarin.Forms;

using System.Collections.ObjectModel;
using System.Windows.Input;

namespace JeNaiJamais
{
	public class PlayViewModel : ViewModelBase
	{
		private string mCurrentPlayer;
		private string mCurrentJnp;
		private PlayerModel mSelectedItem;
		private ObservableCollection<PlayerModel> mListPlayerNo;
		private ObservableCollection<PlayerModel> mListPlayerYes;

		public PlayViewModel()
		{
			ListNameYes = new ObservableCollection<PlayerModel>(DependencyService.Get<IGameService>().getList());
			ListNameNo = new ObservableCollection<PlayerModel>();
			nextAction();
		}

		public string Player 
		{ 
			get { return mCurrentPlayer; }
			set { SetProperty<string>(ref mCurrentPlayer, value);}
		}

		public string JNP 
		{
			get { return mCurrentJnp; }
			set { SetProperty<string>(ref mCurrentJnp, value);}
		}

		public ObservableCollection<PlayerModel> ListNameYes
		{
			get { return mListPlayerYes; }
			set { SetProperty<ObservableCollection<PlayerModel>>(ref mListPlayerYes, value);}
		}

		public ObservableCollection<PlayerModel> ListNameNo
		{
			get { return mListPlayerNo; }
			set { SetProperty<ObservableCollection<PlayerModel>>(ref mListPlayerNo, value);}
		}

		public PlayerModel SelectedPlayerYes
		{
			get { return mSelectedItem; }
			set
			{
				if (SetProperty<PlayerModel>(ref mSelectedItem, value))
				{
					if (value != null)
					{
						mListPlayerNo.Add(mSelectedItem);
						mListPlayerYes.Remove(mSelectedItem);
					}
				}
			}
		}

		public PlayerModel SelectedPlayerNo
		{
			get { return mSelectedItem; }
			set
			{
				if (SetProperty<PlayerModel>(ref mSelectedItem, value))
				{
					if (value != null)
					{
						mListPlayerNo.Remove(mSelectedItem);
						mListPlayerYes.Add(mSelectedItem);
					}
				}
			}
		}


		public ICommand NextCommand 
		{ 
			get { return new Command(nextAction); } 
		}

		public ICommand TakePhotoCommand 
		{ 
			get { return new Command(TakePhotoAction); } 
		}

		private void nextAction()
		{
			foreach(PlayerModel p in mListPlayerNo)
			{
				DependencyService.Get<IGameService>().addAnswer(p, mCurrentJnp, false);
			}
			foreach(PlayerModel p in mListPlayerYes)
			{
				DependencyService.Get<IGameService>().addAnswer(p, mCurrentJnp, true);
			}

			Player = DependencyService.Get<IGameService>().nextPlayer();
			JNP = DependencyService.Get<IGameService>().nextJNP();
		}

		private void TakePhotoAction()
		{
			DependencyService.Get<ICamera>().TakePhoto();
		}
	}
}

