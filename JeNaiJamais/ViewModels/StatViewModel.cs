﻿using Storm.Mvvm;
using Xamarin.Forms;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace JeNaiJamais
{
	public class StatViewModel : ViewModelBase
	{
		private  ObservableCollection<JeNaiJamaisModel> mJeNaiJamaisLst;
		private JeNaiJamaisModel mJnjSelected;
		private string mTotal;
		private string mYes;
		private string mNo;

		public StatViewModel()
		{
			JeNaiJamaisStat = DependencyService.Get<IDatabaseService>().getAllJeNaiJamais(null);
		}

		public ObservableCollection<JeNaiJamaisModel> JeNaiJamaisStat
		{
			get { return mJeNaiJamaisLst; }
			set { SetProperty<ObservableCollection<JeNaiJamaisModel>>(ref mJeNaiJamaisLst, value);}
		}

		public string Total
		{
			get { return mTotal; }
			set { SetProperty<string>(ref mTotal, value);}
		}

		public string Yes
		{
			get { return mYes; }
			set { SetProperty<string>(ref mYes, value);}
		}

		public string No
		{
			get { return mNo; }
			set { SetProperty<string>(ref mNo, value);}
		}


		public JeNaiJamaisModel SelectedJnj
		{
			get { return mJnjSelected; }
			set
			{
				if (SetProperty<JeNaiJamaisModel>(ref mJnjSelected, value))
				{
					if (value != null)
					{
						Total = "Total = " + mJnjSelected.Total;
						Yes = " Total non = " + mJnjSelected.Yes;
						No = "Total oui = " + mJnjSelected.No;
					}
				}
			}
		}

		public ICommand ResetCommand 
		{ 
			get { return new Command(resetAction); } 
		}

		private void resetAction()
		{
			DependencyService.Get<IGameService>().resetAllJnjStat();
			JeNaiJamaisStat = DependencyService.Get<IDatabaseService>().getAllJeNaiJamais(null);
		}
	}
}

