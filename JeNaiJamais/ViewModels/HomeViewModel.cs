﻿using System;
using Storm.Mvvm;
using Xamarin.Forms;
using System.Windows.Input;

namespace JeNaiJamais
{
	public class HomeViewModel : ViewModelBase
	{

		private string mPlayText;  // Play button's text.
		private string mHistoText; // Histo button's text.
		private string mStatText;  // Stat button's text.
		private string mAddText;   // Add button's text.

		public HomeViewModel()
		{
			PlayText = "Jouer";
			HistoText = "Historiques";
			StatText = "Statistiques";
			AddText = "Ajouter un jnj";
		}
			
		/**
		 * Change the play button's text.
		 */ 
		public string PlayText
		{
			get { return mPlayText; }
			set { SetProperty<string>(ref mPlayText, value); }
		}

		/**
		 * Change the histo button's text.
		 */ 
		public string HistoText
		{
			get { return mHistoText; }
			set { SetProperty<string>(ref mHistoText, value); }
		}

		/**
		 * Change the stat button's text.
		 */ 
		public string StatText
		{
			get { return mStatText; }
			set { SetProperty<string>(ref mStatText, value); }
		}

		/**
		 * Change the add button's text.
		 */ 
		public string AddText
		{
			get { return mAddText; }
			set { SetProperty<string>(ref mAddText, value); }
		}

		/**
		 * Call when a user click in play.
		 */ 
		public ICommand PlayCommand
		{
			get { return new Command(PlayAction); }
		}

		/**
		 * Call when a user click in histo.
		 */ 
		public ICommand HistoCommand
		{
			get { return new Command(HistoAction); }
		}

		/**
		 * Call when a user click in stat.
		 */ 
		public ICommand StatCommand
		{
			get { return new Command(StatAction); }
		}

		/**
		 * Call when a user click in add.
		 */ 
		public ICommand AddCommand
		{
			get { return new Command(AddAction); }
		}
			
		/**
		 * Open play page.
		 */ 
		private void PlayAction()
		{
			NavigationService.PushAsync<InitPlayPage>();
		}

		/**
		 * Open histo page.
		 */ 
		private void HistoAction()
		{
			NavigationService.PushAsync<HistoPage>();
		}

		/**
		 * Open stat page.
		 */ 
		private void StatAction()
		{
			NavigationService.PushAsync<StatPage>();
		}

		/**
		 * Open add page.
		 */ 
		private void AddAction()
		{
			NavigationService.PushAsync<AddJnpPage>();
		}
	}
}


