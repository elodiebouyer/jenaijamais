﻿using System;

namespace JeNaiJamais
{
	public interface ICamera
	{
		void TakePhoto();
	}
}

